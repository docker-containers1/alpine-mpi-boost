Alpine Linux based image that contains bash, g++, Open MPI, Boost MPI, and Boost Serialization. Primarily used for CS 4170/5170: Introduction to Parallel Programming at Bowling Green State University when teaching MPI.

To compile code with this image, I suggest the following:

```
docker run --rm -v ${PWD}:/tmp -w {WORKING_DIR} rgreen13/alpine-bash-mpi {COMMAND}
```

To run code:
```
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp .mpiexec --allow-run-as-root -n X MPI
```
where ```X``` is the number of processors used.

To run a makefile in the `Default` directory:
```
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-mpi make all
```