FROM alpine:latest

# MAINTAINER Rob Green <greenr@bgsu.edu>
LABEL maintainer="Robert C. Green II <greenr@bgsu.edu>"

RUN echo "http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN echo "http://nl.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk add --update bash g++ make openmpi openmpi-dev openssh git && rm -r /var/cache/apk/*

RUN mkdir /boost
RUN cd /boost
RUN wget https://boostorg.jfrog.io/artifactory/main/release/1.86.0/source/boost_1_86_0.tar.gz
RUN tar xzf boost_1_86_0.tar.gz
RUN cd boost_1_86_0 && ./bootstrap.sh --with-libraries=mpi,serialization 
RUN cd boost_1_86_0 && echo "using mpi ;" >> project-config.jam
RUN cd boost_1_86_0 &&  ./b2

ENV BOOST_CFLAGS -I/boost_1_86_0/
ENV BOOST_LIBS -L/boost_1_86_0/stage/lib
ENV LD_LIBRARY_PATH /boost_1_86_0/stage/lib